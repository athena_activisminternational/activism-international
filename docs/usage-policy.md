# Usage Policy

Activism Cloud and all other services of Activism.International are meant to be open, accessible and solidary.

We provide free services for people who care for our environment and the climate crisis, who want to make our planet a better place and who want to form a just and sustainable society.

You are free to use Activism Cloud for revolutionary as well as private use, but we ask you to behave fair.

If you do not comply to these values, we do not want you to use our services. If you are unsure, feel free to [contact us](mailto:info@activism.international).