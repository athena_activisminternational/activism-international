# Privacy Policy for collaborative services (if use [Activism Cloud](https://cloud.activism.international), [BigBlueButton](https://bbb.activism.international/b/) or [Matrix (Element)](https://matrix.activism.international))

Privacy is important. That is why we try to reduce the amount of data we collect as much as we can. Data economy means, the fewer data we collect, the fewer data we need to protect.

## Responsibility for data processing

The maintainers listed in the [legal notice](../about.md) are responsible for any processing of personal data.

## Affected users

You are affected by processing of data, as soon as you create an Activism-ID.

In this case, we store the following data:

- your chosen login name
- your chosen display name
- in case provided, your mail address
- the files your upload to our services
- calendars you create
- any other content you actively create in our cloud

## Expiry and protection of stored data

- all files you create in Activism Cloud are being stored encrypted leaving no way for access by thirds
- all messages, documents or images you send in Matrix (Element) are end-to-end encrypted unless you disable encryption leaving no physical way for thirds to access your data
- calendars, polls and surveys you create are stored in plain text and are protected by our clouds access control mechanisms

As your data is yours, we do not decide to delete is unless you violate our usage policy.

You may export, transfer or delete all your data any time in your account settings.

## Reasons for processing data

You actively created content using our online services.

## Transfer to thirds

We only transfer data to thirds, if you ask us to. In no case, we transfer usable or personally identifiable data.

Our cloud supports federated cloud sharing. This means, you can share files, calendars etc. across different online services like Nextcloud, OwnCloud or other compatible cloud services. If you actively create such a share, a notification is sent to the corresponding server. This includes your Activism-ID and your display name.

## Embedded contents

- In Jitsi or BigBlueButton meetings, the moderators can embed other websites, videos or slides into the meeting. As it is up to them which services they integrate, we cannot tell you anything about the services' privacy. Please ask your meeting's moderator to provide the given information.
- In Matrix (Element), users can embed external content into the messaging service. This principle is called *widget*. You are always asked before external content is shown. In case you embed Jitsi or BigBlueButton meetings, [our privacy policy for temporary services applies](temporary.md).

## Cookies

The Activism Cloud uses session cookies to store your login. As these are technically necessary cookies, we do not have to actively ask you for consent.

We do not use advertising or analytics cookies.

## Analytics services

We do not use analytics services.

## Hosting

Our servers are located in the EU and are protected of physical access of thirds.

## Your rights

Even though we try to minimize the personal data we process, we would like to explain you your rights according to GDPR:

You have various rights.

- We fulfill your right on access according to Art. 15. We may provide you a list of all data we have but as most of your data is encrypted, we cannot give you a full list of data we have.
- According to Art. 16, you can request to correct any personal data we collect. In most cases, you can do this yourself in the account settings.
- According to Art. 17, you can request us to erase all personal data of yours. Usually you should be able to complete this yourself in the account settings.
- According to Art. 18, you can request us to decrease the amount or to cherry-pick the kind of personal data we process. As we only store the minimum personal data we require for providing secure access to our services, we physically cannot decrease the amount of personal data we process. This hence means we need to fully delete your account.
- According to Art. 20, you can request us to transfer all personal data we save. As most of your data is encrypted, the only person being able to complete this is you. You can export all your data form the cloud in your account settings. Due to privacy reasons we do not permit export of Matrix (Element) chats as this would break the end-to-end encryption.
- You are allowed to withdraw your consent any time. You can always deny future processing of data.

Independent of the administration or legal orders, you are always free to create an appeal at the competent supervisory authority, especially in the country of abode, your work or the presumed violation, if you assume our processing of personal data violates the GDPR.