# Privacy Policy for the dashboard (if you visit [activism.international](https://activism.international) only)

Privacy is important. That is why we try to reduce the amount of data we collect as much as we can. Data economy means, the fewer data we collect, the fewer data we need to protect.

## Responsibility for data processing

The maintainers listed in the [legal notice](../about.md) are responsible for any processing of personal data.

## Affected users

As we do not collect any data, there is no affected user.

## Reasons for processing data

Due to data economy, we cannot provide a reason for nonexistent data collection.

## Transfer to thirds

As we do not collect any data, we cannot transfer it.

## Embedded contents

There is no embedded content into this webpage.

## Cookies

We do not use cookies.

## Analytics services

We do not use analytics services.

## Hosting

Our servers are located in the EU and are protected of physical access of thirds.

## Your rights

Even though we try to minimize the personal data we process, we would like to explain you your rights according to GDPR:

You have various rights.

- We fulfill your right on access according to Art. 15. You can request a copy of the data we do not collect as an empty mail.
- According to Art. 16, you can request to correct the nonexistent data we collect.
- According to Art. 17, you can request us to erase all personal data of yours. We promise we continuously do this even before you ask us to. If you want a confirmation of this, feel free to email us. We can confirm the automatic erasure by a thumbs up in a mail.
- According to Art. 18, you can request us to decrease the amount or to cherry-pick the kind of data we process. Due to physical reasons, we obviously cannot process less than no data.
- According to Art. 20, you can request us to transfer all data we save (hence nothing)
- You are allowed to withdraw your consent any time. You can always deny future processing of data.

Independent of the administration or legal orders, you are always free to create an appeal at the competent supervisory authority, especially in the country of abode, your work or the presumed violation, if you assume our processing of personal data violates the GDPR.